#

class zulu(
   Array $installed   = ['zulu-13'],
   Array $latest      = [],
   Array $absent      = [],
) {
include zulu::repo

  ensure_packages ( $installed,
    {
      ensure  => 'installed',
      require => Class['zulu::repo'],
    }
  )
  ensure_packages ( $latest,
    {
      ensure  => 'latest',
      require => Class['zulu::repo'],
    }
  )
  ensure_packages ( $absent,
    {
      ensure  => 'absent',
      require => Class['zulu::repo'],
    }
  )
} 
