#Set azul repository

class zulu::repo (
  String $location = 'http://repos.azulsystems.com')
{
  $_os_facts = $facts['os']
  $_os = downcase($_os_facts['name'])

  case $_os_facts['family'] {
    'Debian': {
      apt::source {'zulu_repo':
        comment      => 'repository for zulu products',
        location     => "${location}/${_os}",
        release      => 'stable',
        repos        => 'main',
        key          => {
          id     => '0x27BC0C8CB3D81623F59BDADCB1998361219BD9C9',
          server => 'hkp://keyserver.ubuntu.com:80',
        },
      }
    }
    'RedHat': {
      yumrepo { 'zulu_repo':
        enabled  => 1,
        descr    => 'epository for zulu products',
        baseurl  => 'https://repos.azul.com/zulu/rpm',
        gpgkey   => 'https://assets.azul.com/files/0xB1998361219BD9C9.txt',
        gpgcheck => 1,
        protect  => 1,
      }
    }
  }
}

